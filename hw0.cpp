#include "Angel.h"

const int NumVertices = 36;
typedef Angel::vec4 color4; color4 colors[NumVertices]; color4 vertex_colors[8] = { color4(0.0, 0.0, 0.0, 1.0), color4(1.0, 0.0, 0.0, 1.0), color4(1.0, 1.0, 0.0, 1.0), color4(0.0, 1.0, 0.0, 1.0), color4(0.0, 0.0, 1.0, 1.0), color4(1.0, 0.0, 1.0, 1.0), color4(1.0, 1.0, 1.0, 1.0), color4(0.0, 1.0, 1.0, 1.0) };
typedef Angel::vec4 point4; point4 points[NumVertices]; point4 vertices[8] = { point4(-0.5, -0.5, 0.5, 1.0), point4(-0.5, 0.5, 0.5, 1.0), point4(0.5, 0.5, 0.5, 1.0), point4(0.5, -0.5, 0.5, 1.0), point4(-0.5, -0.5, -0.5, 1.0), point4(-0.5, 0.5, -0.5, 1.0), point4(0.5, 0.5, -0.5, 1.0), point4(0.5, -0.5, -0.5, 1.0) };
int Index = 0;

unsigned int rot = 1; enum {Xaxis = 0, Yaxis = 1, Zaxis = 2, NumAxes = 3}; int Axis = Xaxis; GLfloat Theta[NumAxes] = {0.0, 0.0, 0.0};

GLuint ModelView, Projection; mat4 model_view;

void quad(int a, int b, int c, int d) {
	colors[Index] = vertex_colors[a]; points[Index] = vertices[a]; Index++;
	colors[Index] = vertex_colors[b]; points[Index] = vertices[b]; Index++;
	colors[Index] = vertex_colors[c]; points[Index] = vertices[c]; Index++;
	colors[Index] = vertex_colors[a]; points[Index] = vertices[a]; Index++;
	colors[Index] = vertex_colors[c]; points[Index] = vertices[c]; Index++;
	colors[Index] = vertex_colors[d]; points[Index] = vertices[d]; Index++;
}

void colorcube() {
	Index = 0; quad(1, 0, 3, 2); quad(2, 3, 7, 6); quad(3, 0, 4, 7); quad(6, 5, 1, 2); quad(4, 5, 6, 7); quad(5, 4, 0, 1);
}

void display(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	const vec3 displacement(0.0, 0.0, 0.0);
	model_view = Scale(1.0, 1.0, 1.0) * Translate(displacement) * RotateX(Theta[Xaxis]) * RotateY(Theta[Yaxis]) * RotateZ(Theta[Zaxis]);
	glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
	glDrawArrays(GL_TRIANGLES, 0, NumVertices);
	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {
	if (key == 'q' || key == 'Q') {	exit(EXIT_SUCCESS); }
}

void mouse(int button, int state, int x, int y) {
	if (state == GLUT_DOWN) {
		switch (button) {
			case GLUT_LEFT_BUTTON:   Axis = Xaxis; break;
			case GLUT_MIDDLE_BUTTON: Axis = Yaxis; break;
			case GLUT_RIGHT_BUTTON:  Axis = Zaxis; break;
		}
	}
}

void reshape(int w, int h) {
	glViewport(0, 0, w, h);
	mat4 projection;
	if (w <= h) { projection = Ortho(-1.0, 1.0, -1.0 * (GLfloat)h / (GLfloat)w, 1.0 * (GLfloat)h / (GLfloat)w, -1.0, 1.0);
	} else { projection = Ortho(-1.0 * (GLfloat)w / (GLfloat)h, 1.0 * (GLfloat)w / (GLfloat)h, -1.0, 1.0, -1.0, 1.0); }
	glUniformMatrix4fv(Projection, 1, GL_TRUE, projection);
}

void timer(int p) {
	Theta[Axis] += 5.0;
	if (Theta[Axis] > 360.0) { Theta[Axis] -= 360.0; }
	glutPostRedisplay();
	glutTimerFunc(15.0, timer, 0);
}

int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(512, 512);
	glutInitContextVersion(3, 2);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutCreateWindow("Comp 410 Hw 0");
	glewExperimental = GL_TRUE;
	glewInit();
	colorcube();
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	GLuint buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors) + sizeof(points), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(colors), colors);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(colors), sizeof(points), points);
	GLuint program = InitShader("vshader.glsl", "fshader.glsl");
	GLuint vPosition = glGetAttribLocation(program, "vPosition");
	glEnableVertexAttribArray(vPosition);
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(colors)));
	GLuint vColor = glGetAttribLocation(program, "vColor");
	glEnableVertexAttribArray(vColor);
	glVertexAttribPointer(vColor, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	ModelView = glGetUniformLocation(program, "ModelView");
	Projection = glGetUniformLocation(program, "Projection");
	glUseProgram(program);
	model_view = identity();
	glEnable(GL_DEPTH_TEST);
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutReshapeFunc(reshape);
	glutTimerFunc(5, timer, 0);
	glutMainLoop();
	return 0;
}
